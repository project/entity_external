This module currently integrates with Salesforce to perform CRUD operations on Salesforce objects with Drupal core and Entity API entity functions, including entity_metadata_wrapper.

<h2>Examples:</h2>
<h4>Create a new Salesforce Contact:</h4>
<?php
$entity = entity_create('salesforce_contact', array());

$entity->FirstName = 'John';
$entity->LastName = 'Doe';
$entity->Email = 'jdoe@example.com';

entity_save('salesforce_contact', $entity);
?>

<h4>Delete an existing Salesforce Contact:</h4>
<?php
entity_delete('salesforce_contact', 'sf-id-hash');
?>

<h4>Update an existing Salesforce Contact:</h4>
<code>
$entity = entity_load_single('salesforce_contact', 'sf-id-hash');
$entity_wrapper = entity_metadata_wrapper('salesforce_contact', $entity);
$entity_wrapper->FirstName = 'Joe';
$entity_wrapper->save();</code>
