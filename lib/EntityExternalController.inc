<?php
/**
 * @file
 * Entity External Controller class.
 */

/**
 * EntityExternalController class.
 */
class EntityExternalController extends DrupalDefaultEntityController implements EntityAPIControllerInterface {
  protected $api;

  protected $soql;

  protected $fields;

  protected $externalObjectType;

  protected $objectInfo;

  protected $fieldInfo = array();

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
    if (!empty($this->entityInfo['external object type'])) {
      $this->externalObjectType = $this->entityInfo['external object type'];
    }
    else {
      throw new Exception(t("Entity info property ['external object'] type not defined"));
    }
  }

  /**
   * Get Salesforce API.
   */
  public function api() {
    return salesforce_get_api()->isAuthorized() ? salesforce_get_api() : FALSE;
  }

  /**
   * Run full soql query.
   */
  public function query() {
    $this->soql()->fields = $this->fields();
    return $this->api()->query($this->soql());
  }

  /**
   * Get soql query object.
   */
  protected function soql() {
    if (!isset($this->soql)) {
      $this->soql = new SalesforceSelectQuery($this->externalObjectType);
    }
    return $this->soql;
  }

  /**
   * Convert all IDs to 18 characters.
   *
   * Overrides DrupalDefaultEntityController::cleanIds()
   */
  protected function cleanIds(&$ids) {
    foreach ($ids as &$id) {
      $id = $this->api()->convertId($id);
    }
    parent::cleanIds($ids);
  }

  /**
   * Loads multiple objects from external source with query.
   */
  protected function loadMultiple($ids, $params = array()) {
    $soql = $this->soql();
    if (!empty($ids)) {
      $soql->addCondition('Id', $ids);
    }
    $entity_info = entity_get_property_info($this->entityType);
    // Make sure we only load salesforce fields, as we cannot use custom entity
    // module properties in a soql query.
    $property_info = $this->propertyInfo();
    if (!empty($params['fields'])) {
      $soql->fields = $params['fields'];
      if (!in_array('Id', $soql->fields)) {
        $soql->fields[] = 'Id';
      }
    }
    else {
      $soql->fields = array_keys($property_info);
    }
    if (!empty($params['conditions'])) {
      foreach ($params['conditions'] as $condition) {
        list ($field, $value, $operator) = $condition;
        if (!empty($property_info[$field])) {
          $soql->addCondition($field, $value, $operator);
        }
      }
    }
    if (!empty($params['order'])) {
      list ($field, $value) = $params['order'];
      $soql->order[$field] = $value;
    }

    $results = $this->api()->query($soql);
    $entities = array();
    foreach ($results['records'] as $result) {
      $entities[$result['Id']] = $this->entityClass($result);
    }
    return $entities;
  }

  /**
   * Custom function to call objectRead.
   */
  protected function loadSingle($id, $params = array()) {
    $entity = FALSE;
    if (!$this->api()) {
      drupal_set_message(t('Error: Salesfroce API not authorized.'), 'error');
      return $entity;
    }
    try {
      $entity = $this->api()->objectRead($this->externalObjectType, $id);
    }
    catch (Exception $e) {
      $entity = FALSE;
    }

    if (!empty($entity)) {
      $entity = $this->entityClass($entity);
    }
    else {
      drupal_set_message(t('Salesforce !type object !id not found', array(
        '!id' => $id,
        '!type' => $this->externalObjectType,
      )), 'error');
    }
    return $entity;
  }

  /**
   * Load external entities by external ID.
   *
   * Overrides DrupalDefaultEntityController::load()
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = array();

    // Revisions are not statically cached, and require a different query to
    // other conditions, so separate the revision id into its own variable.
    if ($this->revisionKey && isset($conditions[$this->revisionKey])) {
      $revision_id = $conditions[$this->revisionKey];
      unset($conditions[$this->revisionKey]);
    }
    else {
      $revision_id = FALSE;
    }

    // Create a new variable which is either a prepared version of the $ids
    // array for later comparison with the entity cache, or FALSE if no $ids
    // were passed. The $ids array is reduced as items are loaded from cache,
    // and we need to know if it's empty for this reason to avoid querying the
    // database when all requested entities are loaded from cache.
    $passed_ids = !empty($ids) ? array_flip($ids) : FALSE;
    // Try to load entities from the static cache, if the entity type supports
    // static caching.
    if ($this->cache && !$revision_id) {
      $entities += $this->cacheGet($ids, $this->simplifyConditions($conditions));
      // If any entities were loaded, remove them from the ids still to load.
      if ($passed_ids) {
        $ids = array_keys(array_diff_key($passed_ids, $entities));
      }
    }

    // Ensure integer entity IDs are valid.
    if (!empty($ids)) {
      $this->cleanIds($ids);
    }

    // Load any remaining entities from the database. This is the case if $ids
    // is set to FALSE (so we load all entities), if there are any ids left to
    // load, if loading a revision, or if $conditions was passed without $ids.
    if ($ids === FALSE || $ids || $revision_id || ($conditions && !$passed_ids)) {
      // Build the query.
      $queried_entities = array();

      if (!empty($ids) && is_array($ids) && count($ids) == 1) {
        $id = reset($ids);
        $queried_entities[$id] = $this->loadSingle($id, $conditions);
      }
      else {
        $queried_entities = $this->loadMultiple($ids, $conditions);
      }
    }

    // Pass all entities loaded from the database through $this->attachLoad(),
    // which attaches fields (if supported by the entity type) and calls the
    // entity type specific load callback, for example hook_node_load().
    if (!empty($queried_entities)) {
      $this->attachLoad($queried_entities, $revision_id);
      $entities += $queried_entities;
    }

    if ($this->cache) {
      // Add entities to the cache if we are not loading a revision.
      if (!empty($queried_entities) && !$revision_id) {
        $this->cacheSet($queried_entities);
      }
    }

    // Ensure that the returned array is ordered the same as the original
    // $ids array if this was passed in and remove any invalid ids.
    if ($passed_ids) {
      // Remove any invalid ids from the array.
      $passed_ids = array_intersect_key($passed_ids, $entities);
      foreach (array_filter($entities) as $entity) {
        $passed_ids[$entity->{$this->idKey}] = $entity;
      }
      $entities = $passed_ids;
    }
    return $entities;
  }

  /**
   * Custom create function for external entities.
   */
  public function create(array $values = array()) {
    return $this->entityClass(array());
  }

  /**
   * Custom delete function for external entities.
   */
  public function delete($ids = array()) {
    if (!empty($this->externalObjectType) && !empty($ids)) {
      foreach ($ids as $id) {
        $this->api()->objectDelete($this->externalObjectType, $id);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save($entity) {
    $entity->save();
  }

  /**
   * Get wrapper class for external entities.
   */
  protected function entityClass($array) {
    if (!empty($this->entityInfo['entity class'])) {
      $class = $this->entityInfo['entity class'];
      $entity = new $class($array, $this->entityType);
    }
    else {
      $entity = (object) $array;
    }
    return $entity;
  }

  /**
   * Simplify the conditions for the parent::cacheGet function.
   */
  protected function simplifyConditions(array $conditions) {
    $simple = array();
    if (!empty($conditions['conditions'])) {
      foreach ($conditions['conditions'] as $condition) {
        list ($field, $value,,) = $condition;
        $simple[$field] = trim($value, '\'');
      }
    }
    return $simple;
  }

  /**
   * Get info for current object.
   */
  public function objectInfo() {
    if (empty($this->objectInfo) && $this->api()) {
      $this->objectInfo = $this->api()->objectDescribe($this->externalObjectType);
    }
    return !empty($this->objectInfo) ? $this->objectInfo : array();
  }

  /**
   * Get field info for an external object.
   *
   * @param string $name
   *   Machine name of a field.
   *
   * @return array
   *   Array of field info keyed by field name.
   */
  public function fieldInfo($name = NULL) {
    if (empty($this->fieldInfo)) {
      $info = $this->objectInfo();
      if (!empty($info['fields'])) {
        foreach ($info['fields'] as $field) {
          $this->fieldInfo[$field['name']] = $field;
        }
      }
    }
    return $name && isset($this->fieldInfo[$name]) ? $this->fieldInfo[$name] : $this->fieldInfo;
  }

  /**
   * Get entity property info from external fields.
   */
  public function propertyInfo($name = NULL) {
    $field_info = $this->fieldInfo();
    if (empty($field_info)) {
      return array();
    }
    $property_info = array();
    foreach ($field_info as $key => $field) {
      $property_info[$key] = array(
        'label' => $field['label'],
        'getter callback' => 'entity_property_verbatim_get',
        // Not sure what denotes this is SF.
        'required' => FALSE,
      );
      if ($field['updateable']) {
        if ($field['type'] == 'multipicklist') {
          $property_info[$key]['type'] = 'list<string>';
          $property_info[$key]['getter callback'] = 'external_entity_multipicklist_get';
          $property_info[$key]['setter callback'] = 'external_entity_multipicklist_set';
        }
        else {
          $property_info[$key]['setter callback'] = 'entity_property_verbatim_set';
        }
      }
    }

    return $name && isset($property_info[$name]) ? $property_info[$name] : $property_info;
  }

  /**
   * Get options from salesforce and convert to simple keyed array.
   */
  public function optionsList($field_name) {
    return $this->optionList($field_name);
  }

  /**
   * Get options from salesforce and convert to simple keyed array.
   */
  public function optionList($field_name) {
    if (!isset($field_name)) {
      return array();
    }
    $field_info = $this->fieldInfo($field_name);
    $options = array();
    if (!empty($field_info['picklistValues'])) {
      foreach ($field_info['picklistValues'] as $item) {
        if (!empty($item['active'])) {
          $options[$item['value']] = $item['label'];
        }
      }
    }
    return $options;
  }

  /**
   * Make sure all data passed is a real field, and is updateable.
   */
  public function checkData($data) {
    $field_info = $this->fieldInfo();
    $data = (Object) $data;
    $checked = array();
    foreach ($field_info as $key => $value) {
      if (isset($data->{$key}) && $field_info[$key]['updateable']) {
        $checked[$key] = $data->{$key};
      }
    }
    $properties = entity_get_property_info($this->entityType);
    $properties = reset($properties);
    foreach ($properties as $key => $value) {
      if (isset($data->{$key}) && isset($properties[$key]['type'])) {
        $checked[$key] = $data->{$key};
      }
    }
    return $checked;
  }

  /**
   * {@inheritdoc}
   */
  public function export($entity, $prefix = '') {
    // TODO: needed to implement EntityAPIControllerInterface but not used.
  }

  /**
   * {@inheritdoc}
   */
  public function import($entity, $prefix = '') {
    // TODO: needed to implement EntityAPIControllerInterface but not used.
  }

  /**
   * {@inheritdoc}
   */
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    // TODO: needed to implement EntityAPIControllerInterface but not used.
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($hook, $entity) {
    // TODO: needed to implement EntityAPIControllerInterface but not used.
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL) {
    // TODO: needed to implement EntityAPIControllerInterface but not used.
  }

}
