<?php
/**
 * @file
 * Entity External class.
 */

/**
 * EntityExternal class.
 */
class EntityExternal extends Entity {
  protected $objectType;
  protected $api;
  protected $id = FALSE;
  protected $objectInfo = array();
  protected $fieldInfo = array();

  /**
   * {@inheritdoc}
   */
  public function __construct($values = array(), $entity_type = NULL) {
    $this->api = salesforce_get_api();
    $info = array();
    if (!empty($values['Id'])) {
      $this->id = $values['Id'];
    }
    parent::__construct($values, $entity_type);
    $this->objectType = $this->entityInfo['external object type'];
  }

  /**
   * Get unique id.
   */
  public function getIdentifier() {
    return $this->id;
  }

  /**
   * Get info for current object.
   */
  public function objectInfo() {
    return entity_get_controller($this->entityType)->objectInfo();
  }

  /**
   * Get field info for an external object.
   *
   * @param string $name
   *   Machine name of a field.
   *
   * @return array
   *   Array of field info keyed by field name.
   */
  public function fieldInfo($name = NULL) {
    return entity_get_controller($this->entityType)->fieldInfo($name);
  }

  /**
   * Check if field can be updated.
   */
  public function fieldIsUpdateable($name) {
    $info = entity_get_property_info($this->entityType);
    return !empty($info['properties'][$name]['setter callback']);
  }

  /**
   * Get the label from salesforce for a field.
   *
   * @param string $name
   *   Machine name of a field.
   *
   * @return string
   *   Field label.
   */
  public function getLabel($name) {
    $info = entity_get_controller($this->entityType)->fieldInfo($name);
    return !empty($info['label']) ? $info['label'] : '';
  }

  /**
   * Get field options list.
   *
   * @param string $name
   *   Machine name of a field.
   *
   * @return array
   *   Array of field values.
   */
  public function getOptionsList($name) {
    return entity_get_controller($this->entityType)->optionList($name);
  }

  /**
   * Save the current external entity.
   */
  public function save() {
    $data = entity_get_controller($this->entityType)->checkData($this);
    if (empty($data)) {
      return;
    }
    if ($this->getIdentifier()) {
      $this->api->objectUpdate($this->objectType, $this->getIdentifier(), (array) $data);
    }
    else {
      // Insert.
      $object = $this->api->objectCreate($this->objectType, $data);
      if ($object['success']) {
        $this->{$this->idKey} = $object['id'];
      }
    }
  }
}
